#!/apps/python/2.6/bin/python

## Creates csv files with Quickflash's slicer and 
## plots them with matplotlib

## Submit with following line
# qsub -q normal -l ncpus=1,vmem=400MB,software=hdf5,walltime=0:15:00 smallpyslicer.py

#import matplotlib.pyplot as plt
#import numpy as np
#import matplotlib.cm as cm
#import matplotlib.colors as colors
import os
import glob
import subprocess as sp


## --- Set these -----------------------------------------------------

slicerpath = "/home/100/ayw100/QuickFlash-1.0.0/examples/slicer/"
filespath = "/short/n72/geoff/Jet3D06/compact"
fbasenm = "Jet3D06.plt."
plotvar = "dens"
slicerargs = "--rotate=0,180,90"

## Choose plot type. pcolor is vee-ry slow
plottype = "imshow"
#plottype = "pcolor"

## -------------------------------------------------------------------

fsuffix = ".cvs"
slicercmd = os.path.join(slicerpath,"slicer")



subDirs=['Sel_300', 'Sel_000', 'Sel_200', 'Sel_350', 'Sel_050', 'Sel_100', 'Sel_150', 'Sel_250', 'Sel_400']
subDirs.sort()

for subDir in subDirs:
    files = glob.glob(os.path.join(os.path.join(filespath,subDir),fbasenm+"*"))
    
    for fstr in files:
        sp.Popen([slicercmd,plotvar,fstr]).wait()


#cvsfiles = glob.glob(os.path.join(os.path.join(filespath,subDir),fbasenm+"_"+plotvar+"*"))


f = open(filename,"r")

## Skip header
while (f.readline().strip() != "SliceData"):
    pass

# Read all data
a = f.readlines()
b = [ np.log(map(float,line.strip().split()))/np.log(10) for line in a ]
#b = [ map(float,line.strip().split()) for line in a ]
b.reverse()

Z = np.array(b)

dx, dy = 0.002, 0.002

x = np.arange(0, 1.024, dx)
y = np.arange(-0.512, 0.512, dy)
X,Y = np.meshgrid(x, y)

## pseudo color (slow)
if plottype == "pcolor":
    plt.pcolor(X, Y, Z, norm=colors.LogNorm(vmin=Z.min(),vmax=Z.max()), cmap=cm.jet)
    plt.colorbar()
    plt.axis([-0.512,0.512,0,1])

## image (fast)
if plottype == "imshow":
    #im = plt.imshow(Z, norm=colors.LogNorm(vmin=Z.min(),vmax=Z.max()), cmap=cm.jet, extent=(-0.512,0.512,0,1))
    im = plt.imshow(Z, cmap=cm.jet, extent=(-0.512,0.512,0,1))
    #im.set_interpolation('nearest')
    #im.set_interpolation('bicubic')
    im.set_interpolation('bilinear')
    plt.colorbar()

plt.xlabel("x (kpc)")
plt.ylabel("y (kpc)")
plt.title(r"log($\rho$)")

#plt.show()

plt.savefig("figure.png",format="png")


