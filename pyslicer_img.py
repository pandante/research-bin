#!/apps/python/2.6/bin/python

## Creates csv files with Quickflash's slicer and 
## plots them with matplotlib

## Submit with following line
# qsub -q normal -l ncpus=1,vmem=400MB,software=hdf5,walltime=0:15:00 smallpyslicer.py

## You should be able to speed up this routine by reading data into 
## ndarray directly from perhaps with loadtxt or fromfile

import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cm as cm
import matplotlib.colors as colors
import os
import glob
import subprocess as sp


## --- Set these -----------------------------------------------------

## In future argv use argvs

## path to slicer executable
slicerpath = "/home/100/ayw100/QuickFlash-1.0.0/examples/slicer/"
## path to hdf files
filespath = "/short/n72/geoff/Jet3D06/compact"
## path to csv files 
csvpath = "speed"
fbasenm = "Jet3D06.plt."
## Data variable
datavar = "speed"
## Variable to be plotted
plotvar = "speed"
vectors = True
nth = 10
## Normalization of plot variable
## Plotting range & normalization
## For speed
norm = 1.
varmin = 0.0005*norm
varmax = 1.*norm
## For pressure
#norm = 9.185e-4
#varmin =9.8e-8*norm
#varmax = 8.e-3*norm
## For density
#norm = 1.
#varmin = 6.5e-4*norm
#varmax = 6.5e4*norm

slicerargs = "--rotate=0,180,90"

log = True
img = True
istar2 = False
## orient = "up" | "side"
orient = "side" 

## Range indices. -1 means lowest lower 
## limit or highest upper limit, respectively.
ibegin = 400
iend = 400

## A small number to add to 0 in log scale
## This must be slightly larger than varmax
small = 0.00001*varmin*norm

## -------------------------------------------------------------------


## CSV file suffix
fsuffix = ".csv"

## A separate flag that controls whether
## we're working with logarithm in script
## Used if plotvar == "speed"
logswitch = False

## Just in case
if plotvar != "speed":
  vectors = False



## Do the plotting

logvarmin = np.log(varmin)/np.log(10)
logvarmax = np.log(varmax)/np.log(10)
lognorm = np.log(norm)/np.log(10)

## Stretch factor for i*2 files
varstretch = 32000. 


if plotvar == "speed":
    csvfiles = glob.glob(os.path.join(csvpath,fbasenm)+"*_"+"velx"+fsuffix)
    csvfiles2 = glob.glob(os.path.join(csvpath,fbasenm)+"*_"+"vely"+fsuffix)
    csvfiles3 = glob.glob(os.path.join(csvpath,fbasenm)+"*_"+"velz"+fsuffix)
    csvfiles.sort()
    csvfiles2.sort()
    csvfiles3.sort()
else:
    #csvfiles = glob.glob(os.path.join(os.path.join(filespath,subDir),fbasenm+"_"+plotvar+"*"))
    csvfiles = glob.glob(os.path.join(csvpath,fbasenm)+"*_"+plotvar+fsuffix)
    csvfiles.sort()





## Default indices
if ibegin == -1:
    ibegin = 0
if iend == -1:
    iend = len(csvfiles) - 1


#for csvfile in csvfiles[ibegin:iend+1]:
for ifile in range(ibegin,iend+1):
    
    ## Open file(s)
    f = open(csvfiles[ifile],"r")
    
    if plotvar == "speed":
        f2 = open(csvfiles2[ifile],"r")
        f3 = open(csvfiles3[ifile],"r")
    
    ## Skip header
    while (f.readline().strip() != "SliceData"):
        pass
    if plotvar == "speed":
        while (f2.readline().strip() != "SliceData"):
            pass
        while (f3.readline().strip() != "SliceData"):
            pass
    
    
    ## Read all data
    a = f.readlines()
    if plotvar == "speed":
        a2 = f2.readlines()
        a3 = f3.readlines()
    
    ## Temporarily switch off log for plotvar "speed"
    if plotvar == "speed" and log :
        log = False
        logswitch = True
    
    ## "if line.strip()"  removes empty lines (e.g. at end of file)
    ## log
    if log:
        b = [ np.log(map(float,line.strip().split()))/np.log(10) for line in a if line.strip()]
    ## linear
    else:
        b = [ map(float,line.strip().split()) for line in a if line.strip()]
        if plotvar == "speed":
            b2 = [ map(float,line.strip().split()) for line in a2 if line.strip()]
            b3 = [ map(float,line.strip().split()) for line in a3 if line.strip()]
    
    if orient == "up":
        b.reverse()
        if plotvar == "speed":
            b2.reverse()
            b3.reverse()
    
    ## Switch on log again for plotvar "speed"
    if plotvar == "speed" and logswitch :
        log = True
        logswitch = False
    
    if img:
        if log:
            if plotvar == "speed":
                Z = np.log(np.sqrt(np.square(np.array(b)) + np.square(np.array(b2)) + np.square(np.array(b3)) + small))/np.log(10) + lognorm
            else:
                Z = np.array(b) + lognorm
        else:
            if plotvar == "speed":
                Z = np.sqrt(np.square(np.array(b)) + np.square(np.array(b2)) + np.square(np.array(b3)))*norm
            else:
                Z = np.array(b)*norm
        
        if orient == "side":
            Z = np.transpose(Z)
        
        ## Create mesh
        ## For pseudocolor. Not really using this with img.
        #dx, dy = 0.002, 0.002
        #x = np.arange(0, 1.024, dx)
        #y = np.arange(-0.512, 0.512, dy)
        #X,Y = np.meshgrid(x, y)
        
        ## For vectors.
        dx, dy = 1.024*nth/512, -1.024*nth/512
        x = np.arange(0, 1.024, dx)
        y = np.arange(0.512, -0.512, dy)
        X,Y = np.meshgrid(x, y)
        
        ## Figure and axes for imshow 
        fig = figure(1)
        ax = fig.add_subplot(111)
        
        ## image (fast)
        ## Is there any way to orient the image with the axes?
        if orient == "up":
            ## some color schemes: PuOr_r, jet, Set1 
            im = plt.imshow(Z, cmap=cm.jet, norm=colors.Normalize(vmin=logvarmin,vmax=logvarmax), extent=(-0.512,0.512,0,1))
        if orient == "side":
            im = plt.imshow(Z, cmap=cm.jet, norm=colors.Normalize(vmin=logvarmin,vmax=logvarmax), extent=(0,1,-0.512,0.512))
        
        #im.set_interpolation('nearest')
        #im.set_interpolation('bicubic')
        im.set_interpolation('bilinear')
        plt.colorbar()
        
        ## Adjust axes and depending on orientation
        if orient == "up":
            plt.xlabel("y (kpc)")
            plt.ylabel("x (kpc)")
        if orient == "side":
            plt.xlabel("x (kpc)")
            plt.ylabel("y (kpc)")
        
        if plotvar == "dens":
            plt.title(r"log($\rho$)")
        if plotvar == "pres":
            plt.title(r"log($p$ / dyn cm$^{-2}$)")
        if plotvar == "speed":
            plt.title(r"log($|v| / c$)")
        
        #plt.show()
        
        ## Vector plot
        if plotvar == "speed" and vectors:
            ## Only use every nth value in either direction
            c1 = [ [ b[irow][ielem] for ielem in range(len(b[irow])) if ielem % nth == 0 ] for irow in range(len(b)) if irow % nth == 0 ]
            c2 = [ [ b2[irow][ielem] for ielem in range(len(b2[irow])) if ielem % nth == 0 ] for irow in range(len(b2)) if irow % nth == 0 ]
            c3= [ [ b3[irow][ielem] for ielem in range(len(b3[irow])) if ielem % nth == 0 ] for irow in range(len(b3)) if irow % nth == 0 ]
            npa1 = np.array(c1)
            npa2 = np.array(c2)
            npa3 = np.array(c3)
            ## Scaling
            ## default
            scale12 = 1
            ## Log scaling
            #scale12 = np.log10(sqrt(square(npa1) + square(npa2)) + small)
            #scale12 = scale12 - scale12.min() 
            ## Equal scaling (not good because can see very small vectors)
            #scale12 = sqrt(square(npa1) + square(npa2)) + small
            #scale12 = 1./scale12

            ## y-dimension sign must be flipped because we've got the mesh wrong way up
            Z1 = np.maximum(np.minimum(npa1*norm*scale12,0.3),0.0001)
            Z2 = np.maximum(np.minimum(-npa2*norm*scale12)
            #Z1 = npa1*norm*scale12
            #Z2 = -npa2*norm*scale12
            Z3 = npa3*norm
            if orient == "side":
                Z1 = np.transpose(Z1)
                Z2 = np.transpose(Z2)
                Z3 = np.transpose(Z3)
            
            ## Figure and axes for imshow 
            #fig = figure(1)
            #ax = fig.add_subplot(111)
            
            qv = plt.quiver(X, Y, Z1, Z2, pivot='middle')
            #qv = plt.quiver(Z1, Z2, pivot='middle')
        
        
        if plotvar == "speed":
            if vectors:
                plt.savefig((csvfiles[ifile].replace(fsuffix,".png")).replace("velx","speed+vectors"),format="png",dpi=None)
            else:
                plt.savefig((csvfiles[ifile].replace(fsuffix,".png")).replace("velx","speed"),format="png",dpi=None)
        else:
            plt.savefig(csvfiles[ifile].replace(fsuffix,".png"),format="png",dpi=None)
        
        plt.close(fig)
        
        f.close()
    
    if istar2:
        if plotvar == "speed":
            print("Support for variable \"speed\" not added yet.")
        if vectors:
            print("Error: Vectors not possible in I*2.")
            
        
        fint = open(csvfiles[ifile].replace(fsuffix,".raw"),"wb")
        ## Nested list comprehensions: convert b to i*2, then to one long string
        ## % 2**16 converts to unsigned 16-bit
        if log:
            c = [[ int(round((elem-logvarmin)/(varmax-logvarmin)*varstretch))%2**16 for elem in line ] for line in b ]
            Zint = np.array(c) + lognorm
        else:
            c = [[ int(round((elem-varmin)/(varmax-varmin)*varstretch))%2**16 for elem in line ] for line in b ]
            Zint = np.array(c)*lognorm
        
        if orient == "side":
            Zint = np.transpose(Zint)
        
        oneline = Zint.reshape(1,512*512)
        oneline.tofile(fint,sep="")
        ## You can read in with np.fromfile(file,dtype=int,sep="") again
        fint.close()
    

