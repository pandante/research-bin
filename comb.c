#include <iostream>
#include <fstream>

#define NJ 65536
#define HD 18

using namespace std;

int main(int argc, char * argv[]){

  // Data vars
  char junk[NJ];
  int size;

  // File handles
  ifstream fi;
  ofstream fo;

  // Check number of arguments
  if (argc < 3){
    cout << ("Error: Wrong number of arguments. Must be > 1.");
    return 1;
  }



  // Loop over input files
  for (int j=0; j<argc-2; ++j){ 

    // Open files for read and write
    fi.open(argv[1+j], fstream::in | fstream::binary);

    // Read and ignore HD header lines
    for (int i=0; i<HD; ++i) fi.getline(junk, NJ);

    // Number of points
    fi >> junk >> size;
    size *= 4;

    // New line and empty lines
    fi.getline(junk, NJ); fi.getline(junk, NJ);

    // Copy next two lines (in text mode)
    fo.open(argv[argc-1],  fstream::app);
    sprintf(junk, "");
    fi.getline(junk, NJ); fo << junk << endl;
    fi.getline(junk, NJ); fo << junk << endl;
    fo.close();


    // Copy rest of file (binary mode)
    fo.open(argv[argc-1],  fstream::app | fstream::binary);
    char * data = new char [size];
    fi.read(data, size); fo.write(data, size); fo << endl;
    delete[] data;
    fo.close();


    // Close input file
    fi.close();

  }

  return 0;
}


